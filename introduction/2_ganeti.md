!SLIDE bullets transition=fade

# What I will cover #

* Ganeti terminology, comparisons, & goals
* Cluster & virtual machine setup
* Dealing with outages
* OSUOSL usage of ganeti
* Future roadmap
