!SLIDE small-img center transition=fade

# Ganeti #
## Creating a low-cost clustered virtualization environment ##
### by Lance Albertson ###

![osllogo](osllogo.png)

!SLIDE bullets transition=fade

# About Me #

* OSU Open Source Lab
* Server hosting for Open Source projects
* Lead Systems Administrator / Architect
* Gentoo developer / contributor
* Jazz trumpet performer 
