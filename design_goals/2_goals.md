!SLIDE bullets transition=fade

# Goals #

* Reduce hardware cost
* Increase service availability
* Increase management flexibility
* Administration transparency

!SLIDE bullets transition=fade

# Principles #

* Not dependent on specific hardware
* Scales linearly
* Single node takes admin master role
* N+1 redundancy
