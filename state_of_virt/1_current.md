!SLIDE bullets transition=fade

# State of Virtualization #

* Citrix XenServer
* libvirt: oVirt, virt-manager
* Eucalyptus
* VMWare
* Open Stack*

!SLIDE bullets transition=fade

# Issues #

* Overly complicated
* Lack of HA Storage integration
* Not always 100% open source 
* Multiple layers of software
