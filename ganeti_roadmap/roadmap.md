!SLIDE bullets transition=fade

# Open source #

* http://code.google.com/p/ganeti/
* License: GPL v2
* Ganeti 1.2.0 - December 2007
* Ganeti 2.0.0 - May 2009
* Ganeti 2.1.0 - March 2010 / 2.1.7 current
* Ganeti 2.2.0 - Oct 2010 / 2.2.1 current

!SLIDE smbullets transition=fade

# Ganeti roadmap #

* Inter-cluster instance moves
* KVM security (currently in >= 2.1.2.1)
* Cluster LVM support
* LXC (Linux containers)
* Node Groups
* Cluster state Caching
* IPv6 Support
